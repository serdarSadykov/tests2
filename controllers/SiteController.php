<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Reports;
use yii\data\ActiveDataProvider;
use yii\base\UserException;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login','logout','index','add'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','index','add'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index',[
			'reports' => new ActiveDataProvider([
				'query'		=> Yii::$app->user->getModel()->getReports(),
				'sort'		=> [
					'defaultOrder' => [
						'datet'=>SORT_DESC
					]
				],
				'pagination'=> [
					'pageSize' => 50,
				],
			])
		]);
    }

    /**
     * Report detail action.
     *
     * @return string
     */
    public function actionDetail(){
		$ID = Yii::$app->request->get('ID');
		if(!$ID) throw new NotFoundHttpException(Yii::t('app',"Report not found"));
		$report = Yii::$app->user->getModel()->getReports()->where(['ID'=>$ID])->one();
		if(!$report) throw new NotFoundHttpException(Yii::t('app',"Report not found"));
		
		$chartData = [
			[Yii::t('app','Ticket'),Yii::t('app','Balance')]
		];

		$csum = 0;
		foreach($report->reportDatas as $report_data){
			if(
				$report_data->Type == $report::TYPE_BUY ||
				$report_data->Type == $report::TYPE_BALANCE
			){
				//$beg_price	= (float)$report_data->Price;
				//$end_price	= (float)$report_data->Price2;
				//$coef			= (float)$report_data->Size;
				//$profit		= -round(($beg_price-$end_price)*($coef*100000),2);
			
				$profit		=	(float)$report_data->Profit;
				$csum		+=	$profit;
				$chartData[] = 
					[Yii::t('app',"Ticket: {ticket}. Datetime: {datetime}. {profit}",[
						'ticket'	=> $report_data->Ticket,
						'profit'	=> $profit,
						'datetime'	=> $report_data->Type == $report::TYPE_BUY
							?($report_data->CloseTime	?Yii::$app->formatter->asDateTime($report_data->CloseTime)	:"")
							:($report_data->OpenTime	?Yii::$app->formatter->asDateTime($report_data->OpenTime)	:"")
					
					]),$csum];
			}
		}

        return $this->render('detail',[
			'report' 	=> $report,
			'chartData' => $chartData,
			'reportData'=> new ActiveDataProvider([
				'query'		=> $report->getReportDatas(),
				'pagination'=> false,
			])
		]);
	}
    /**
     * Add report action.
     *
     * @return string
     */
    public function actionAdd(){
		$nReport = new Reports();
		if(Yii::$app->request->isPost){
			try{
				$nReport->file = UploadedFile::getInstanceByName('Reports[file]');
				if(!$nReport->save()){
					throw new UserException(\yii\helpers\Html::errorSummary($nReport, ['class' => 'errors']));
				}
				return $this->redirect(\yii\helpers\Url::to(['detail','ID'=>$nReport->ID]));
			}catch(UserException $exc){
				$nReport->addError('file',strip_tags($exc->getMessage()));
			}
		}
        return $this->render('add',[
			'report'=>$nReport
		]);
	}
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
