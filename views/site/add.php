<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
$this->title = "Add report";
?>
<div class="site-add">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields:</p>

	<?= Html::errorSummary($report, ['class' => "errors"]) ?>
	<?php $form = ActiveForm::begin(); ?>

	<?=$form->field($report, 'file')->fileInput(['accept' => $report::AVAIBLE_FORMATS]);?>

	<div class="form-group">
		<?=Html::submitButton('Add',[
			'class'		=> "btn btn-success",
			'onclick'	=> "this.textContent='".Yii::t('app',"Sending")."';this.setAttribute('disabled','disabled')"
		]);?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
