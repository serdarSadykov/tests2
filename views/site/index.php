<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'Your reports';
?>
<div class="site-index">
    <div class="body-content">
	<form action="<?=Url::to(['detail'])?>">
		<h1><?= Html::encode($this->title) ?></h1>
		<?= GridView::widget([
			'dataProvider' => $reports,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'datet:datetime',
				[
					'label'		=> Yii::t('app',"Action"),
					'format'	=> 'raw',
					'value'		=> function($data){
						return Html::a(Yii::t('app',"Detail"),Url::to(['detail','ID'=>$data->ID]),['class'=>"btn btn-primary"]);
					}
				],
				/*[
					'label'		=> Yii::t('app',"Compare"),
					'format'	=> 'raw',
					'value'		=> function($data){
						return Html::checkbox('ID['.$data->ID.']',false,['class'=>"form-control compareCheckbox"]);
					},
				],*/
			],
			'rowOptions'   => function($model, $key, $index, $grid){
				return ['data-id' => $model->ID];
			},
		]); ?>
		<a href="<?=Url::to(['add'])?>" class="btn btn-primary"><?=Yii::t('app',"Add")?> <span class="glyphicon glyphicon-plus"></span></a>
		<?/*=Html::button(Yii::t('app',"Compare"),[
			'class'		=> "btn btn-default",
			'onclick'	=> "var _form = $(this).closest('form'); if(_form.find('.compareCheckbox:checked').length>1){_form.submit()}else{alert('".Yii::t('app',"Nothing 2 compare")."')}"
		])*/?>
	</form>
    </div>
</div>
