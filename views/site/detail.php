<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use scotthuangzl\googlechart\GoogleChart;

$this->title = Yii::t('app','Report: {report}',['report'=>Yii::$app->formatter->asDateTime($report->datet)]);
?>
<div class="site-error">
    <h1><?=Html::encode($this->title) ?></h1>
<?=GoogleChart::widget(['visualization' => 'LineChart',
	'data'		=> $chartData,
	'options'	=> [
		'title'			=> '',
		'titleTextStyle'=> ['color' => '#FF0000'],
		'vAxis'			=> [
			'title'		=> $chartData[0][1],
			'gridlines' => [
				'color' => 'rgba(0,0,0,.2)' 
			]
		],
		'hAxis'		=> ['title' => $chartData[0][0]],
		//'curveType'	=> 'function',
		'legend'	=> ['position' => 'bottom'],
		'explorer'	=> [
			'maxZoomOut'	=> 2,
			'keepInBounds'	=> true
		],
		'chartArea' => [
			'width'	=>"90%",
			'height'=>"60%"
		],
		'annotations'=>[
			'textStyle'=>[
				'bold'=>false
			]
		],
		'tooltip'=>[
			'textStyle'=>[
				'bold'=>false
			]
		]
	]]);
?>
<br/>
<br/>
<div><?
$csum = 0;
echo GridView::widget([
		'dataProvider' => $reportData,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'Ticket',
			'OpenTime:datetime',
			'Type',
			'Note',
			'Size',
			'Item',
			'Price',
			'S_L',
			'T_P',
			'CloseTime:datetime',
			'Price2',
			'Commission',
			'Taxes',
			'Swap',
			'Profit',
			[
				'label'		=> Yii::t('app',"Balance"),
				'format'	=> 'raw',
				'value'		=> function($data) use (&$csum,&$report){
					if($data->Type == $report::TYPE_BUY || $data->Type == $report::TYPE_BALANCE){
						$profit	=	(float)$data->Profit;
						$csum	+=	$profit;
						return $csum;
					}
					return NULL;
				},
			],
		],
	]); 
	?>
	
</div>
</div>
