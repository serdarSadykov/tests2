<?
namespace app\components;
use app\models\Users;

class User extends \yii\web\User{
	private $_user;
	public $isAdmin;
	public $isAgent;
	public $isOperator;
	public $isBuhgalter;
	public $role;

	function init(){
		$this->_user = Users::findIdentity($this->id);

		if($this->_user){
		}
	}

	function getAttributeLabel($title){
		return $this->_user?$this->_user->getAttributeLabel($title):NULL;
	}

	function getModel(){
		return $this->_user?$this->_user:NULL;
	}
}?>