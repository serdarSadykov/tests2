<?
namespace app\components;

use yii\web\Controller;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;
use Yii;

class CController extends Controller
{
	public function beforeAction($action){
		if(Yii::$app->request->isAjax){
			Yii::$app->assetManager->bundles = [
				'yii\bootstrap\BootstrapPluginAsset' => false,
				'yii\bootstrap\BootstrapAsset' => false,
				'yii\web\JqueryAsset' => false,
				'yii\web\YiiAsset' => false,
			];
		}
		
		if(parent::beforeAction($action)){
			if(Yii::$app->user->isGuest) return $this->goHome();
			return true;
		}
		return false;
	}
	
    public function init() {
        parent::init();
    }
}