<?php

namespace app\models;
use darkdrim\simplehtmldom\SimpleHTMLDom as SHD;
use yii\base\UserException;
use Yii;

/**
 * This is the model class for table "reports".
 *
 * @property integer $ID
 * @property string $datet
 * @property integer $user
 *
 * @property ReportData[] $reportDatas
 * @property Users $user0
 */
class Reports extends \yii\db\ActiveRecord
{
	const AVAIBLE_FORMATS	= "html";
	const TYPE_BALANCE		= "balance";
	const TYPE_BUY			= "buy";
	const TYPE_BUY_STOP		= "buy stop";
	public $file;
	public $cReportDatas;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datet'], 'safe'],
            [['user'], 'integer'],
			[['user'], 'default', 'value' => Yii::$app->user->getId()],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user' => 'ID']],
			[['file'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => self::AVAIBLE_FORMATS, 'maxFiles' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes){
		parent::afterSave($insert, $changedAttributes);
		try{
			foreach($this->cReportDatas as $item){
				$item->report = $this->ID;
				if(!$item->save()) throw new UserException(\yii\helpers\Html::errorSummary($item));
			}
		}catch(UserException $exc){
			$this->addError('file', Yii::t('app',strip_tags($exc->getMessage())));
		}
	}
    /**
     * @inheritdoc
     */
	public function beforeSave($insert)
	{
		if(!parent::beforeSave($insert)) return false;

		return $this->parseFile();
	}
    /**
     * Parse file
     */
	private function parseFile(){
		$this->cReportDatas = [];
		$fileData			= NULL;
		try{
			$tableBegun	= false;
			$fileData	= SHD::str_get_html(file_get_contents($this->file->tempName));
			foreach($fileData->find('table tbody tr') as $tr){
				if($tableBegun){
					if(!$tr->children(0) || $tr->children(0)->getAttribute('colspan')  == "10") break;
					
					$item = NULL;
					$type = $tr->children(2);
					if(!$type) throw new UserException("File format error");
					
					switch($type->plaintext){
						case self::TYPE_BUY_STOP:
						case self::TYPE_BUY:
							$item = $this->parseBuy($tr);
							break;
						case self::TYPE_BALANCE:
							$item = $this->parseBalance($tr);
							break;
						default:
							throw new UserException("Unknown field type ".$type->plaintext." [".count($this->cReportDatas)."]. Supports:".self::TYPE_BUY.",".self::TYPE_BALANCE.",".self::TYPE_BUY_STOP);
					}
					if(!$item) throw new UserException("File format error");
					$this->cReportDatas[] = $item;
				}else{
					$tableBegun = $tr->getAttribute('bgcolor') == "#C0C0C0";
				}
			}
			if(!count($this->cReportDatas)) throw new UserException("No data in file");
		}catch(UserException $exc){
			$this->addError('file', Yii::t('app',$exc->getMessage()));
			return false;
		}
		if($fileData){
			$fileData->clear();
			unset($fileData);
		}
		return true;
	}
    /**
     * Parse buy line
     */
	private function parseBuy($tr){
		$fields = [
			'Ticket'		=> $tr->children(0)? (int)$tr->children(0)->plaintext							:NULL,
			'OpenTime'		=> $tr->children(1)? $tr->children(1)->plaintext									:NULL,
			'Type'			=> $tr->children(2)? $tr->children(2)->plaintext									:NULL,
			'Item'			=> $tr->children(4)? $tr->children(4)->plaintext									:NULL,
			'CloseTime'		=> $tr->children(8)? $tr->children(8)->plaintext									:NULL,
			'Size'			=> $tr->children(3)? Yii::$app->helper->parseFloat($tr->children(3)->plaintext)		:NULL,
			'Price'			=> $tr->children(5)? Yii::$app->helper->parseFloat($tr->children(5)->plaintext)		:NULL,
			'S_L'			=> $tr->children(6)? Yii::$app->helper->parseFloat($tr->children(6)->plaintext)		:NULL,
			'T_P'			=> $tr->children(7)? Yii::$app->helper->parseFloat($tr->children(7)->plaintext)		:NULL,
			'Price2'		=> $tr->children(9)? Yii::$app->helper->parseFloat($tr->children(9)->plaintext)		:NULL,
			'Commission'	=> $tr->children(10)?Yii::$app->helper->parseFloat($tr->children(10)->plaintext)	:NULL,
			'Taxes'			=> $tr->children(11)?Yii::$app->helper->parseFloat($tr->children(11)->plaintext)	:NULL,
			'Swap'			=> $tr->children(12)?Yii::$app->helper->parseFloat($tr->children(12)->plaintext)	:NULL,
			'Profit'		=> $tr->children(13)?Yii::$app->helper->parseFloat($tr->children(13)->plaintext)	:NULL
		];
		return $this->reportDataGenerate($fields);
	}
    /**
     * Parse balance line
     */
	private function parseBalance($tr){
		$fields = [
			'Ticket'	=> $tr->children(0)?(int)$tr->children(0)->plaintext							:NULL,
			'OpenTime'	=> $tr->children(1)?	 $tr->children(1)->plaintext							:NULL,
			'Type'		=> $tr->children(2)?	 $tr->children(2)->plaintext							:NULL,
			'Note'		=> $tr->children(3)?	 $tr->children(3)->plaintext							:NULL,
			'Profit'	=> $tr->children(4)?Yii::$app->helper->parseFloat($tr->children(4)->plaintext)	:NULL,
		];

		return $this->reportDataGenerate($fields);
	}
    /**
     * Generate reportData
     */
	private function reportDataGenerate($fields){
		$result = NULL;
		try{
			$newReportData = new ReportData();
			foreach($fields as $f_name=>$f_val){
				$newReportData->$f_name = $f_val;
			}

			if(!$newReportData->validate())
				throw new UserException(\yii\helpers\Html::errorSummary($newReportData));
			
			$result = $newReportData;
		}catch(UserException $exc){
			$this->addError('file', Yii::t('app',strip_tags($exc->getMessage())));
		}
		return $result;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID'	=> Yii::t('app', 'ID'),
            'datet'	=> Yii::t('app', 'Datet'),
            'user'	=> Yii::t('app', 'User'),
            'file'	=> Yii::t('app', 'Report file'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportDatas()
    {
        return $this->hasMany(ReportData::className(), ['report' => 'ID'])->orderBy(['ID'=>SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(Users::className(), ['ID' => 'user']);
    }
}
