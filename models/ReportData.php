<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reportData".
 *
 * @property integer $ID
 * @property integer $report
 * @property integer $Ticket
 * @property string $OpenTime
 * @property string $Type
 * @property string $Note
 * @property double $Size
 * @property string $Item
 * @property double $Price
 * @property double $S_L
 * @property double $T_P
 * @property string $CloseTime
 * @property double $Price2
 * @property double $Commission
 * @property double $Taxes
 * @property double $Swap
 * @property double $Profit
 *
 * @property Reports $report0
 */
class ReportData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reportData';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report', 'Ticket'], 'integer'],
            [['OpenTime', 'CloseTime'], 'safe'],
            [['Size', 'Price', 'S_L', 'T_P', 'Price2', 'Commission', 'Taxes', 'Swap', 'Profit'], 'number'],
            [['Ticket'], 'required'],
            [['Item','Type'], 'string', 'max' => 50],
            [['Note'], 'string', 'max' => 255],
            [['report'], 'exist', 'skipOnError' => true, 'targetClass' => Reports::className(), 'targetAttribute' => ['report' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'report' => Yii::t('app', 'Report'),
            'Ticket' => Yii::t('app', 'Ticket'),
            'OpenTime' => Yii::t('app', 'Open  Time'),
            'Type' => Yii::t('app', 'Type'),
            'Note' => Yii::t('app', 'Note'),
            'Size' => Yii::t('app', 'Size'),
            'Item' => Yii::t('app', 'Item'),
            'Price' => Yii::t('app', 'Price'),
            'S_L' => Yii::t('app', 'S / L'),
            'T_P' => Yii::t('app', 'T / P'),
            'CloseTime' => Yii::t('app', 'Close  Time'),
            'Price2' => Yii::t('app', 'Price2'),
            'Commission' => Yii::t('app', 'Commission'),
            'Taxes' => Yii::t('app', 'Taxes'),
            'Swap' => Yii::t('app', 'Swap'),
            'Profit' => Yii::t('app', 'Profit'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport0()
    {
        return $this->hasOne(Reports::className(), ['ID' => 'report']);
    }
}
