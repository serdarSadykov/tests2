<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@frontend/messages',
					'sourceLanguage' => 'en',
					'fileMap' => [
					],
				],
			],
		],
		'formatter' => [
			'class'				=> 'yii\i18n\Formatter',
			'dateFormat'		=> 'php:d.m.Y',
			'datetimeFormat'	=> 'php:d.m.Y H:i:s',
			'timeFormat'		=> 'php:H:i:s',
            'currencyCode'		=> 'RUR',
			'timeZone'			=> 'Europe/Moscow',
			'defaultTimeZone'	=> 'Europe/Moscow',
		],
		'helper' => [
		  'class' => 'app\components\Helper',
		],
        'request' => [
            'cookieValidationKey' => 'dIP-fF3e_9sxkX7TlSwwqngEL_Y8JqPr',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
			'class'				=> 'app\components\User',
            'identityClass'		=> 'app\models\Users',
            'enableAutoLogin'	=> true,
			'loginUrl'			=> ['/site/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
		'allowedIPs' => ['*.*.*.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
		'allowedIPs' => ['127.0.0.1']//['*.*.*.*'],
    ];
}

return $config;
